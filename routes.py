from datetime import datetime, timedelta
from flask import (
    Blueprint, jsonify, request
)

from .db import get_db
from .models.user import User
from .models.session import Session
from .models.log import Log

bp = Blueprint('routes', __name__)


@bp.route('/user/get/<id>', methods=['GET'])
def user_get(id):

    user = User.get(id)

    if user is not None:
        user = dict(user)
        del user['password']
        return jsonify({'code': 200, 'user': user})

    return jsonify({'code': 500, 'message': 'There is no user with this id'})


@bp.route('/user/create', methods=['POST'])
def user_create():

    username = request.form['username']
    password = request.form['password']

    error = None

    if not username:
        error = 'Username is required.'
    elif not password:
        error = 'Password is required.'
    elif User.get_by_username(username) is not None:
        error = 'A user with the username {} is already registered.'.format(username)

    if error is None:
        User.create(username, password)
        return jsonify({'code': 200})

    return jsonify({'code': 500, 'message': error})


@bp.route('/session/close/<id>', methods=['GET'])
def close_session(id):

    session = Session.get(id)

    if session is not None:
        Session.close(id)
        return jsonify({'code': 200})

    return jsonify({'code': 500, 'message': 'There is no session with this id'})


@bp.route('/logs/store', methods=['POST'])
def store_logs():

    logs = request.get_json()

    nullUserIds = []

    # if there is no session id given, start a new session
    for i in range(0, len(logs)):
        userId = logs[i]['userId']

        if User.get(userId) is None:
            nullUserIds.append(i)
        elif logs[i]['sessionId'] is None:
            logs[i]['sessionId'] = Session.getSessionForUser(userId)

    # remove logs that are not associated with a valid user
    for id in nullUserIds:
        del logs[id]

    Log.store_multiple(logs)

    return jsonify({'code': 200})


@bp.route('/logs/retrieve', methods=['POST'])
def retrieve_logs():

    error = None

    # apply date filters
    startDate = None
    endDate = None
    if 'start_date' in request.form and 'end_date' in request.form:
        startDate = time_string_to_datetime(request.form['start_date'])
        endDate = time_string_to_datetime(request.form['end_date'])

    # apply log type filters
    logType = None
    if 'log_type' in request.form:
        if request.form['log_type'] not in ['VIEW', 'NAVIGATE', 'CLICK']:
            error = 'Please enter a valid log type'
        logType = request.form['log_type']

    # apply user filters
    userId = None
    if 'user_id' in request.form:
        if User.get(request.form['user_id']) is None:
            error = 'There is no user with this id.'
        userId = request.form['user_id']

    if error is None:
        logs = Log.retrieve(startDate, endDate, logType, userId)
        return jsonify({'code': 200, 'logs': logs})
    return jsonify({'code': 500, 'message': error})


def time_string_to_datetime(timeString):

    timeLocal = timeString[:-6]
    utcOffset = timeString[-6:]
    offsetSign = -1 if utcOffset[0] == '-' else 1

    time = datetime.strptime(timeLocal.replace('T', ' '), '%Y-%m-%d %H:%M:%S')

    time += timedelta(hours=offsetSign * int(utcOffset[1:3]))
    time += timedelta(minutes=offsetSign * int(utcOffset[4:]))

    return time