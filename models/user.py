from flask import Flask
from ..db import get_db
from werkzeug.security import check_password_hash, generate_password_hash

app = Flask(__name__)


class User:

    @staticmethod
    def get(id):
        with app.app_context():
            db = get_db()
        return db.execute('SELECT * FROM users WHERE id=(?)', (id,)).fetchone()

    @staticmethod
    def get_by_username(username):
        with app.app_context():
            db = get_db()
        return db.execute('SELECT * FROM users WHERE username=(?)', (username,)).fetchone()

    @staticmethod
    def create(username, password):
        with app.app_context():
            db = get_db()
        db.execute(
            'INSERT INTO users (username, password) VALUES (?, ?)',
            (username, generate_password_hash(password))
        )
        db.commit()
        return True
