from flask import Flask
from ..db import get_db

app = Flask(__name__)


class Session:

    @staticmethod
    def get(sessionId):
        with app.app_context():
            db = get_db()
        return db.execute('SELECT * FROM sessions WHERE id=(?)', (sessionId,)).fetchone()

    @staticmethod
    def getSessionForUser(userId):
        with app.app_context():
            db = get_db()
        existingSession = db.execute('SELECT * FROM sessions WHERE userId=(?) AND closedAt IS NULL', (userId,)).fetchone()
        if existingSession is not None:
            print('existing session')
            return existingSession[0]
        else:
            print('new session')
            db.execute(
                'INSERT INTO sessions (userId) VALUES (?)',
                (userId,)
            )
            sessionId = db.execute('SELECT last_insert_rowid()').fetchone()[0]
            db.commit()
            return sessionId

    @staticmethod
    def close(sessionId):
        with app.app_context():
            db = get_db()
            db.execute(
                'UPDATE sessions SET closedAt=CURRENT_TIMESTAMP WHERE id=(?)',
                (sessionId,)
            )
            db.commit()
        return True

