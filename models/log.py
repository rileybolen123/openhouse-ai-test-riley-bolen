from flask import Flask
from datetime import datetime, timedelta
from ..db import get_db

app = Flask(__name__)


class Log:

    @staticmethod
    def store_multiple(logs):
        with app.app_context():
            db = get_db()
        for log in logs:
            db.execute(
                'INSERT INTO logs (sessionId) VALUES (?)',
                (log['sessionId'],)
            )
            logId = db.execute('SELECT last_insert_rowid()').fetchone()[0]
            for action in log['actions']:
                locationX = None
                locationY = None
                viewedId = None
                pageFrom = None
                pageTo = None
                if action['type'] == 'CLICK':
                    locationX = action['properties']['locationX']
                    locationY = action['properties']['locationY']
                elif action['type'] == 'VIEW':
                    viewedId = action['properties']['viewedId']
                elif action['type'] == 'NAVIGATE':
                    pageFrom = action['properties']['pageFrom']
                    pageTo = action['properties']['pageTo']
                else:
                    continue

                time = time_string_to_datetime(action['time'])

                db.execute(
                    """
                    INSERT INTO actions 
                    (time, type, logId, locationX, locationY, viewedId, pageFrom, pageTo) 
                    VALUES (?,?,?,?,?,?,?,?)
                    """,
                    (time, action['type'], logId, locationX, locationY, viewedId, pageFrom, pageTo)
                )
        db.commit()
        return True

    @staticmethod
    def retrieve(startDate, endDate, logType, userId):
        with app.app_context():
            db = get_db()
        queryString = 'SELECT logs.*, sessions.userId FROM logs LEFT JOIN sessions on logs.sessionId=sessions.id'

        if logType is not None:
            queryString += " LEFT JOIN actions on logs.id=actions.logId"

        queryString += " WHERE TRUE"

        if userId is not None:
            queryString += " AND sessions.userId='{0}'".format(userId)

        if logType is not None:
            queryString += " AND actions.type = '{0}'".format(logType)

        if startDate is not None and endDate is not None:
            queryString += "AND logs.createdAt BETWEEN '{0}' AND '{1}'".format(startDate, endDate)

        queryString += ' ORDER BY logs.createdAt DESC'

        logs = [dict(row) for row in db.execute(queryString).fetchall()]
        for row in logs:
            actionQueryString = "SELECT * FROM actions WHERE logId='{0}'".format(row['id'])
            if logType is not None:
                actionQueryString += " AND type='{0}'".format(logType)
            actionQueryString += " ORDER BY time DESC"
            row['actions'] = [dict(row) for row in db.execute(actionQueryString).fetchall()]
        return logs


def time_string_to_datetime(timeString):
    timeLocal = timeString[:-6]
    utcOffset = timeString[-6:]
    offsetSign = -1 if utcOffset[0] == '-' else 1
    time = datetime.strptime(timeLocal.replace('T', ' '), '%Y-%m-%d %H:%M:%S')
    time += timedelta(hours=offsetSign * int(utcOffset[1:3]))
    time += timedelta(minutes=offsetSign * int(utcOffset[4:]))
    return time