# openhouse.ai coding test Riley Bolen #


### Local testing instructions ###

* clone project
* cd into root directory
* run `export FLASK_APP=__init__.py`
* run `export FLASK_ENV=development`
* run `flask init-db`
* run `flask run`

### API Documentation ###

All POST fields should be submitted using formData unless otherwise noted. All endpoints return JSON

##### GET `/user/get/{userId}`
return a user by id

##### POST `/user/create`
create a new user
* username (string) required
* password (string) required

##### GET `/session/close/{sessionId}`
close a session

##### POST `/logs/store`
upload new logs to be stored.
Logs should be submitted to this endpoint as a raw request body of type `application/json`. Should be an array of JSON objects, in the same format as the sample log that was given.

##### POST `/logs/retrieve`
retrieve a collection of logs.

* start_date (string with format YYYY-MM-DDTHH:mm:ss+00:00) optional

* end_date (string with format YYYY-MM-DDTHH:mm:ss+00:00) optional

* log_type (string, one of ['VIEW', 'NAVIGATE', 'CLICK']) optional

* user_id (integer) optional

### Notes

I chose to use autoincrement ids for the tables, instead of alpha numeric as in the sample log.

The sessions are created automatically when logs are submitted, and must be closed manually. A new session will be opened if there is no open session for a user when their logs are submitted, and the sessionId field on their log is null. The session will remain open until it is closed with the `/session/close` endpoint.

### Follow Up Question

I think that one of the biggest changes that would have to be made for this solution to be cloud scalable would be the database. SQLite was my choice due to the time constraints here, but in a real solution I would be interested in trying out AWS DynamoDB, with the app running on an AWS ec2 instance. I have used a setup like this before with a nodeJS websocket driven app, and it performed quite well. 